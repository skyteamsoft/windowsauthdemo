﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WindowsAuth.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : ControllerBase
    {
        [AllowAnonymous]
        [HttpGet]
        public string Get()
        {
            return "Service works!";
        }

        // Rolleri gormek için sunucuda command line ile aşağıdaki komutu vermelisin.
        // net user DSMESVER /domain
        [Authorize]
        [Route("role/{roleName}")]
        public IActionResult GetRole(string roleName)
        {
            var isInRole = User.IsInRole(roleName);
            return Ok($"{roleName}:{isInRole}");
        }
    }
}
